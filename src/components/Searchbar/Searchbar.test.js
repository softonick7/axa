import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Home from './../../pages/Home';



Enzyme.configure({ adapter: new Adapter() });

describe('TEST SearchBar Component -->', () => {

    it('Navigation to list ', () => {
        const historyMock = { push: jest.fn() };
        const home = shallow(<Home history={historyMock} />);
        home.find('SearchBar').getElements()[0].props.onSearch.call(this,"hi");
        expect(historyMock.push.mock.calls[0]).toEqual([("/list/hi")]);
    });

    it('Navigation to list ', () => {
        const historyMock = { push: jest.fn() };
        const home = shallow(<Home history={historyMock} />);
        home.find('SearchBar').getElements()[0].props.onSearch.call(this,"hi");
        expect(historyMock.push.mock.calls[0]).toEqual([("/list/hi")]);
    });
});


