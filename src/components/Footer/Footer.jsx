import React from 'react';
import style from './Footer.module.css';

class Footer extends React.Component {
    render() {
        return (
            <div className={style.Footer}>© Sofian Azouache</div>
        )
    }
}

export default Footer;