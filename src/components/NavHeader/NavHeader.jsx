import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import SearchBar from '../Searchbar/SearchBar';
import { withRouter } from 'react-router';
import logo from './../../media/images/axa_logo.jpg';
import style from './NavHeader.module.css';

const r = require("../../routes");

class NavHeader extends React.Component {

    constructor() {
        super();
        this.state = {
            selectedTab: 0
        }
        this.onSearch = this.onSearch.bind(this);
    }

    onSearch(tileEntityId) {
        const path = r.inhabitantsList(tileEntityId);
        this.props.history.push(path);
    }

    render() {
        return (
            <Navbar className={style.Header} collapseOnSelect expand="lg" variant="dark">
                <Navbar.Brand href="./../">
                    <img
                        src={logo}
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                        alt="Business Job Logo"
                    />{' '}
                    AXA Gnome Network
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto" />
                    <SearchBar onSearch={this.onSearch} />
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

export default withRouter(NavHeader);
