import React, { Component } from 'react';
// import { InputRange } from 'react-bootstrap';
import './RangeSlider.css';


class RangeSlider extends Component {

    constructor(props) {
        super(props);

        this.onChangeMax = this.onChangeMax.bind(this);
    }

    onChangeMax(e) {
        let max = e.target.value;
        let min = e.target.parentElement.children[1].value;
        let realValue = Math.max(max, min - (-1));
        e.target.value = realValue.toString();
        realValue = (realValue / parseInt(e.target.max)) * 100;
    }

    render() {
        return (
            <input type="range" value="1000" max="1000" min="0" step="1" onInput={this.onChangeMax} />
        );
    }
}

export default RangeSlider;