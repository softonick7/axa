import React, { Component } from 'react';
import { FormControl, InputGroup, Dropdown, DropdownButton, Card, Row, Accordion, ButtonGroup } from 'react-bootstrap';
import Style from './FilterBar.module.css';
import { Button } from 'react-bootstrap';

class FilterBar extends Component {

    constructor(props) {
        super(props);

        this.state = {
        };
        this.onChange = this.onChange.bind(this);
        this.onClear = this.onClear.bind(this);
    }

    onClear(e) {
        this.props.onClear();
    }

    onChange(e, f) {
        var name, value;
        if (e) {
            name = e.target.name;
            value = e.target.value;
        } else if (f) {
            name = f.target.name;
            value = f.target.text;
        }
        this.props.onChange(value, name);
    }

    render() {

        const colors = ["Pink", "Red", "White", "Black", "Green", "Gray", "Blue"];
        const colorsOptions = colors.map((color) => {
            return (<Dropdown.Item name="hairColor" key={color} style={{ background: color, color: color !== "Black" ? "Black" : "white" }}>{color}</Dropdown.Item>);
        });
        const roles = ["Potter", "Medic", "Cook", "Butcher", "Smelter", "Tinker", "Baker", "Miner", "Leatherworker", "Farmer", "Mechanic", "Carpenter", "Gemcutter", "Brewer", "Stonecarver", "Marble Carver", "Tax inspector", "Blacksmith", "Metalworker", "Sculptor", "Prospector", "Mason", "Tailor"];
        const rolesOptions = roles.map((rol) => {
            return (<Dropdown.Item name="rol" key={rol}>{rol}</Dropdown.Item>);
        })

        return (
            <>
                <Accordion defaultActiveKey="1" style={{ margin: "1rem" }}>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            Filter
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="0">
                            <div>
                                <div style={{ margin: "1rem" }}>
                                    <InputGroup className="mb-3">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text>Friends</InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl name="friends" onChange={this.onChange} />
                                    </InputGroup>
                                </div>
                                <div className={Style.FilterContainer}>
                                    <div className={Style.FilterGroup}>
                                        <InputGroup className="mb-3">
                                            <InputGroup.Prepend>
                                                <InputGroup.Text>First name</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <FormControl name="name" onChange={this.onChange} />
                                            <InputGroup.Prepend>
                                                <InputGroup.Text>Last name</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <FormControl name="lastname" onChange={this.onChange} />
                                        </InputGroup>
                                        <InputGroup className="mb-3">
                                            <InputGroup.Prepend>
                                                <InputGroup.Text>Min age</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <FormControl name="ageMin" onChange={this.onChange} />
                                            <InputGroup.Prepend>
                                                <InputGroup.Text>Max age</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <FormControl name="ageMax" onChange={this.onChange} />
                                        </InputGroup>
                                        <InputGroup className="mb-3">
                                            <InputGroup.Prepend>
                                                <InputGroup.Text>Min Height</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <FormControl name="minHeight" onChange={this.onChange} />
                                            <InputGroup.Prepend>
                                                <InputGroup.Text>Min Weight</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <FormControl name="minWeight" onChange={this.onChange} />
                                        </InputGroup>
                                    </div>
                                    <div className={Style.FilterGroup}>
                                        <InputGroup className="mb-3">
                                            <DropdownButton onSelect={this.onChange} id="colorsDD" style={{ width: "100%" }} drop="right" alignRight as={ButtonGroup} title="Hair Color" variant="light" >
                                                {colorsOptions}
                                            </DropdownButton>
                                        </InputGroup>
                                        <InputGroup className="mb-3">
                                            <DropdownButton onSelect={this.onChange} id="rolDD" style={{ width: "100%" }} drop="down" alignRight as={ButtonGroup} title="Rol" variant="light" >
                                                {rolesOptions}
                                            </DropdownButton>
                                        </InputGroup>
                                        <InputGroup className="mb-3">
                                            <Row style={{ width: "inherit", margin: "auto" }}>
                                                <Button size="sm" style={{ width: "100%" }} onClick={this.onClear} >Clear</Button>
                                            </Row>
                                        </InputGroup>
                                    </div>
                                </div>
                            </div>
                        </Accordion.Collapse>
                    </Card>
                </Accordion>
            </>
        );
    }
}

export default FilterBar;