import React from 'react';
import Enzyme, {
    shallow
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import RolCard from './RolCard';



Enzyme.configure({
    adapter: new Adapter()
});

describe('TEST SearchBar Component -->', () => {
            it('Card creation : ', () => {
                    var data = {
                        name: "Test",
                        lastname: "Unit",
                        age: 45,
                        friends: null,
                        hairColor: "Black",
                        Weight: 20,
                        Height: 100.0,
                        professions: ['Cook']
                    };
                    const rolCard = shallow( < RolCard inhabitants = {
                            data
                        }
                        inhabitant = {
                            data
                        }
                        />);
                    
                        expect(rolCard.children().props().children[0].props.alt).toEqual("Test");


                    });
            });