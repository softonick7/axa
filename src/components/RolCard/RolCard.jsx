import React, { Component } from 'react';
import { Card, Image, Row, Toast } from 'react-bootstrap';
import Style from './RolCard.module.css';
import LazyLoad from 'react-lazyload';
import Pacman from 'react-spinners/PacmanLoader';


class RolCard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            friend: null
        };
    }

    onFriendClick() {
        if (this.state.friend) {
            let e = this.state.friend[0];
            return (
                <Toast style={{ height: "150px" }} onClose={() => this.setState({ friend: null })}>
                    <Toast.Header>
                        <Row>
                            <img src={e.thumbnail} height={50} className="rounded mr-2" alt={e.name} />
                            <strong className="mr-auto">{e.name}</strong>
                        </Row>
                        <small>{e.age + " years"}</small>
                    </Toast.Header>
                    <Toast.Body>Let's create a social Gnome network!</Toast.Body>
                </Toast>
            );
        } else {
            return;
        }
    }

    render() {

        const { inhabitant } = this.props;
        let friends, professions;
        if (inhabitant) {
            professions = inhabitant.professions.map((profession, ind, array) => {
                if (ind !== array.length - 1) {
                    return profession.toString().concat(" - ");
                } else {
                    return profession.toString();
                }
            });

            friends = !inhabitant.friends ? null : inhabitant.friends.map((friend, ind) => {
                var t = this.props.inhabitants.filter((a) => { return a.name === friend });
                return (
                    <Image key={friend.concat(ind)}
                        onClick={() => {
                            setTimeout(() => { this.setState({ friend: null }) }, 3000);
                            this.setState({ friend: t })
                        }}
                        className={Style.roundImg}
                        roundedCircle
                        width={50}
                        height={50}
                        alt={friend}
                        src={t[0].thumbnail}
                    />
                );
            });
        } else {
            professions = null;
            friends = null;
        }
        return (
            <LazyLoad once offset={200} placeholder={<Row><Pacman color="orangered" css={{ margin: "auto", marginTop: "10%" }} /></Row>}>
                {this.onFriendClick()}
                <Card className={`${Style.card} "bg-dark text-white"`} key={inhabitant.id}>
                    <Card.Img variant="top" src={inhabitant.thumbnail} alt={inhabitant.name} />
                    <Card.Body className={Style.cardBody}>
                        <Card.Title>{inhabitant.name}</Card.Title>
                        <Card.Text>
                            {professions}
                        </Card.Text>
                    </Card.Body>
                    {friends &&
                        <Card.Footer className={Style.CardFooter} style={{ backgroundColor: inhabitant.hair_color, color: inhabitant.hair_color === "Black" ? "White" : "Black", display: 'inline-flex', overflowX: "auto" }}>
                            <b>Friends:</b>
                            {friends}
                        </Card.Footer>
                    }
                </Card>
            </LazyLoad>
        );
    }
}

export default RolCard;