var r = require("../routes");

export const apiService = {
    get,
};

function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
};

function get() {

    const requestOptions = {
        mode: 'cors',
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        },
        responseType: 'json'
        // headers: authHeader()
    }
    return fetch(r.api, requestOptions)
        .then(handleErrors)
        .then(response => response.json());
};