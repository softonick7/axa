module.exports = {

    /* ROUTES */
    inhabitantsList: (name) => {
        return `/list/${name}`
    },

    /* API */
    api: "/rrafols/mobile_test/master/data.json",
}