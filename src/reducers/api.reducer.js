const initialState = {
    requesting: false,
    items: [],
};

export function getApiStatus(state = initialState, action) {
    switch (action.type) {
        case "API_GETALL_SUCCESS":
            return Object.assign({}, {
                ...state,
                requesting: false,
                items: action.items
            });
        case "API_GETALL_REQUEST":
            return Object.assign({}, {
                ...state,
                requesting: true
            });
        case "API_GETALL_FAILURE":
            return Object.assign({}, {
                ...state,
                requesting: false,
                error: action.error.message
            });
        default:
            return state;
    }
}