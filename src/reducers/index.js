import {
    combineReducers
} from "redux";
import {
    getApiStatus
} from "./api.reducer";

const rootReducer = combineReducers({
    getApiStatus,
});

export default rootReducer;