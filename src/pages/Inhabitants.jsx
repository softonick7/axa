import React, { Component, lazy, Suspense } from 'react';
import { connect } from 'react-redux';
import { apiActions } from '../actions';
import FilterBar from '../components/FilterBar/FilterBar';
import Style from './Inhabitants.module.css';
import Pacman from 'react-spinners/PacmanLoader';

const CardList = lazy(() => import('../components/CardList/CardList'));

class Inhabitants extends Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.onChange = this.onChange.bind(this);
        this.onClear = this.onClear.bind(this);
    }

    componentDidMount() {
        const { match: { params } } = this.props;
        this.setState({ name: params.id });
        this.props.get();
    }

    onClear() {
        this.setState({
            name: null,
            lastname: null,
            ageMin: null,
            ageMax: null,
            friends: null,
            hairColor: null,
            minWeight: null,
            minHeight: null,
            rol: null
        });
    }

    onChange(value, name) {
        this.setState({
            [name]: value
        });
    }

    onFilter() {
        let inhabitants = this.props.inhabitants.Brastlewark;

        if (!inhabitants) {
            return;
        }

        if (this.state.name) {
            inhabitants = inhabitants.filter((inhabitant) => {
                return ((inhabitant.name && inhabitant.name.toLowerCase().includes(this.state.name.toLowerCase())));
            });
        }

        if (this.state.lastname) {
            inhabitants = inhabitants.filter((inhabitant) => {
                return ((inhabitant.name && inhabitant.name.toLowerCase().includes(this.state.lastname.toLowerCase())));
            });
        }

        if (this.state.hairColor) {
            inhabitants = inhabitants.filter((inhabitant) => {
                return (inhabitant.hair_color.includes(this.state.hairColor));
            });
        }
        if (this.state.friends) {
            inhabitants = inhabitants.filter((inhabitant) => {
                return (inhabitant.friends.includes(this.state.friends));
            });
        }
        if (this.state.rol) {
            inhabitants = inhabitants.filter((inhabitant) => {
                return (inhabitant.professions.includes(this.state.rol));
            });
        }
        if (this.state.ageMin) {
            inhabitants = inhabitants.filter((inhabitant) => {
                return (inhabitant.age > this.state.ageMin);
            });
        }

        if (this.state.ageMax) {
            inhabitants = inhabitants.filter((inhabitant) => {
                return (inhabitant.age < this.state.ageMax)
            });
        }

        if (this.state.minWeight) {
            inhabitants = inhabitants.filter((inhabitant) => {
                return (parseFloat(inhabitant.weight) > parseFloat(this.state.minWeight));
            });
        }

        if (this.state.minHeight) {
            inhabitants = inhabitants.filter((inhabitant) => {
                return (parseFloat(inhabitant.height) > parseFloat(this.state.minHeight))
            });
        }

        return inhabitants;
    }

    render() {
        const inhabitantsFiltered = this.onFilter();
        const inhabitants = this.props.inhabitants.Brastlewark;
        return (
            <div className={Style.Container}>
                <h1 style={{ color: "white", marginTop: "5rem" }}>Brastlewark Inhabitants</h1>
                {inhabitantsFiltered &&
                    <>
                        <FilterBar onChange={this.onChange} onClear={this.onClear} />
                        <Suspense fallback={<Pacman color="orangered" size="100px" css={{ margin: "auto", marginTop: "10%" }} />}>
                            {inhabitants && <CardList inhabitantsFiltered={inhabitantsFiltered} inhabitants={inhabitants}></CardList>}
                        </Suspense>
                    </>
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    inhabitants: state.getApiStatus.items,
});

const mapDispatchToProps = dispatch => ({
    get: (id) => dispatch(apiActions.get(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Inhabitants);
