import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

describe('TEST SearchBar Component -->', () => {

test('Check NavHeader exists :', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/AXA Gnome Network/i);
  expect(linkElement).toBeInTheDocument();
});

test('Check Footer exists :', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/© Sofian Azouache/i);
  expect(linkElement).toBeInTheDocument();
})

});
