# AXA

## Design

This web app is designed with the standarts guidelines and reusing or modifing Bootstrap Components.

## Stack of Technologies

For this project I used React framework, HTML5 , EMA6, CSS

## Workflow

At the beginning, the app shows the home page, if the customer wants, we can use that page to start loading the info from server.
In that page we can type in a searchbar, the name of our Gnome or just click on the search button to see all inhabitants.
The list is pretty simple, the info is shown as Card with all the information inside. The color of the footer is the color of hair.
The main part here, is the filter, you can filter by profession, friend, name...
The loading of the component and the images are different, specially for mobile devices.

## Test cases:

I create 2 examples of Component Unit Test, 1 of them is related to routering and the other with the rendering of information.
Normally are always the same cases.

## External Libraries

* React Bootstrap
* React Redux 
* React Lazy load 
* React Logger
* React Thunk 
* Enzyme
* Jest


## Remaining Topics 

* Create a language translations with @Lingui
* Add PWA with code changes in the manifest and ServiceWorker
* Local storage for save most visited info in a simple JSON 
* Add icon and Social Media, for sharing.
* Improve the performance of the lazy and the filters by code splitting and another techniques.
* Improve responsives design.
* Change some Text inputs by some Double range Slider.



_______________________________________________________________________________________________________________________________________________________________





This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
=======
# AXA

